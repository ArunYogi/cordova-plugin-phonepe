var exec = require('cordova/exec');

exports.shouldShow = function (options, success, error) {
    exec(success, error, 'PhonePeSDKPlugin', 'shouldShow', [options]);
};

exports.startPaymentTransaction = function (options, success, error) {
    exec(success, error, 'PhonePeSDKPlugin', 'startPaymentTransaction', [...options]);
};
