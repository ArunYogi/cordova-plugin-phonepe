package com.cordova.phonepe;

import android.content.Intent;
import com.phonepe.intent.sdk.api.PhonePe;
import com.phonepe.intent.sdk.api.PhonePeInitException;
import com.phonepe.intent.sdk.api.ShowPhonePeCallback;
import com.phonepe.intent.sdk.api.TransactionRequest;
import com.phonepe.intent.sdk.api.TransactionRequestBuilder;
import org.apache.cordova.CordovaPlugin;
import org.apache.cordova.CallbackContext;
import org.json.JSONArray;
import org.json.JSONException;
import java.util.HashMap;
import static android.app.Activity.RESULT_CANCELED;
import static android.app.Activity.RESULT_OK;
/**
 * This class echoes a string called from JavaScript.
 */
public class PhonePeSDKPlugin extends CordovaPlugin {
    /**
     * Global variables
     */
    public static int PHONEPE_REQUEST = 777;
    public static CallbackContext uiCallbackHandler;
    @Override
    public boolean execute(String action, JSONArray data, CallbackContext uiCallbackHandler)
    throws JSONException {
        // Initiate PhonePe SDK
        PhonePe.init(cordova.getContext());
        this.uiCallbackHandler = uiCallbackHandler;
        if (action.equalsIgnoreCase("shouldShow")) {
            try {
                PhonePe.isUPIAccountRegistered(new ShowPhonePeCallback() {
                    @Override
                    public void onResponse(boolean b) {
                        uiCallbackHandler.success("" + b);
                    }
                });
            } catch (PhonePeInitException e) {
                e.printStackTrace();
            }
            return true;
        } else if (action.equalsIgnoreCase("startPaymentTransaction")) {
            String base64Body = data.getString(0);
            String checksum = data.getString(1);
            String apiEndPoint = data.getString(2);
            String xCallbackUrl = data.getString(3); // Optional parameter
            TransactionRequest mTransactionRequest;
            HashMap < String, String > headers = new HashMap();
            headers.put("X-CHANNEL-ID", "phonepe_cordova_sdk");
            if (xCallbackUrl != null && xCallbackUrl.length() != 0 && !xCallbackUrl.isEmpty()) {
                headers.put("X-CALLBACK-URL", xCallbackUrl);
                headers.put("X-CALL-MODE", "POST");
                mTransactionRequest = new TransactionRequestBuilder()
                    .setData(base64Body)
                    .setChecksum(checksum)
                    .setUrl(apiEndPoint)
                    .setHeaders(headers)
                    .build();
            } else {
                mTransactionRequest = new TransactionRequestBuilder()
                    .setData(base64Body)
                    .setChecksum(checksum)
                    .setHeaders(headers)
                    .setUrl(apiEndPoint)
                    .build();
            }
            try {
                cordova.startActivityForResult(this,
                    PhonePe.getTransactionIntent(mTransactionRequest), PHONEPE_REQUEST);
            } catch (PhonePeInitException e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        if (requestCode == PHONEPE_REQUEST) {
            if (resultCode == RESULT_OK) {
                this.uiCallbackHandler.success("User Completed");
            } else if (resultCode == RESULT_CANCELED) {
                this.uiCallbackHandler.success("User Cancelled");
            }
        }
    }
}